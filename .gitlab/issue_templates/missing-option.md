`systemd` has a unit file option "`XXX`", an equivalent of which is missing from `rinit`.

`systemd` documentation: https://www.freedesktop.org/software/systemd/man/systemd.directives.html#XXX=

/assign @levex
/label ~"A\-configuration" ~"P\-medium"
/milestone %"Open Sourcing" 
