#!/bin/sh

# Run this script to generate the directory structure required to create
# the initrd. This script should run only once.
#
# Copyright 2018 (c) Levente Kurusa <lkurusa@acm.org>
# This file is licensed under either (1) the MIT License; or
# (2) the Apache v2.0 License, at your option.

mkdir -p m/{dev,bin,etc,usr,proc,sbin,sys,var}
mkdir -p m/usr/{sbin,bin,lib}
mkdir -p m/usr/share/rinit/services
mkdir -p m/var/log/services/
touch m/etc/mdev.conf
cd m
ln -s bin/rinit init
cd ..
cd m/bin
ln -s busybox sh
cd ../..
