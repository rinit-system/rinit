#!/bin/sh

# Run this script to prepare an initrd.igz for a new round of testing.
# This script pulls the cargo-built executable automatically.
#
# Copyright 2018 (c) Levente Kurusa <lkurusa@acm.org>
# This file is licensed under either (1) the MIT License; or
# (2) the Apache v2.0 License, at your option.

[[ -e initrd.igz ]] && rm initrd.igz

cp ../target/x86_64-unknown-linux-musl/debug/rinit m/bin/rinit
cp ../target/x86_64-unknown-linux-musl/debug/rctl m/bin/rctl
cp ../target/x86_64-unknown-linux-musl/debug/rinit-dbus-monitor m/bin/rinit-dbus-monitor
cp ../target/x86_64-unknown-linux-musl/debug/rinit-file-monitor m/bin/rinit-file-monitor
cp ${BUSYBOX_PATH}/busybox m/bin/busybox

cp ../sample-configs/*.toml m/usr/share/rinit/
cp ../sample-configs/services/*.toml m/usr/share/rinit/services

cd m
find . | cpio -H newc -o > ../_tmp_initrd.cpio
cd ..
cat _tmp_initrd.cpio | gzip > initrd.igz
rm _tmp_initrd.cpio
