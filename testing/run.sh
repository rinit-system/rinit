#!/bin/sh

# This script runs the kernel with the current initrd.
#
# Copyright 2018 (c) Levente Kurusa <lkurusa@acm.org>
# This file is licensed under either (1) the MIT License; or
# (2) the Apache v2.0 License, at your option.

qemu-system-x86_64 \
	-kernel linux-bzImage.bin \
	-initrd initrd.igz \
	-monitor null \
	-serial stdio \
	-nographic \
	-no-reboot \
	-netdev tap,id=net0,ifname=rinit_tap0,script=no,downscript=no \
	-device e1000,netdev=net0,mac=52:55:00:13:37:42 \
	-append "earlyprintk=ttyS0 console=ttyS0 RUST_BACKTRACE=1"
