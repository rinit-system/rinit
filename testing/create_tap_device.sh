#!/bin/bash

# This script creates a TAP device known as "rinit_tap0".
# This script should run once per the host machine boots.
# Before running, set ${ETHERNET_DEVICE} to your ethernet device, as
# shown in `ip addr show`.
#
#
# Copyright 2018 (c) Levente Kurusa <lkurusa@acm.org>
# This file is licensed under either (1) the MIT License; or
# (2) the Apache v2.0 License, at your option.

brctl addbr rinit_br0
ip addr flush dev enp4s0
brctl addif rinit_br0 enp4s0
tunctl -t rinit_tap0 -u `whoami`
brctl addif rinit_br0 rinit_tap0
ifconfig enp4s0 up
ifconfig rinit_tap0 up
ifconfig rinit_br0 up
dhclient -v rinit_br0
