extern crate inotify;
extern crate libc;
extern crate futures;

use inotify::{EventMask, WatchMask, Inotify};
use futures::stream::Stream;

use std::path::Path;

fn check_if_pid_alive(target_pid: i32) {
    unsafe {
        let res = libc::kill(target_pid, 0);
        if res == -1 {
            std::process::exit(1);
        }
    }
}

pub fn main() {
    let args = std::env::args().collect::<Vec<_>>();
    let target_path = Path::new(&args[1]);
    let target_pid = &args[2].parse::<i32>();
    let mut inotif_buffer = [0; 1024];

    /* First, quickly check if we can short circuit */
    if target_path.exists() {
        println!("rinit-file-monitor: path {:?} exists already, exiting", target_path);
        std::process::exit(0);
    }

    let mut inotif = Inotify::init()
        .expect("Error initializing inotify, is this Linux?");

    inotif.add_watch(target_path.parent().unwrap(), WatchMask::MODIFY | WatchMask::CREATE)
        .expect(&format!("Error setting INOTIFY_CREATE watch on {:?}, permissions?", target_path));

    loop {
        let events = inotif.read_events(&mut inotif_buffer)
            .expect("Failed to read inotify fd");
        for event in events {
            if event.mask.contains(EventMask::CREATE) {
                match event.name {
                    None => (),
                    Some(name) => {
                        if name.to_string_lossy() == target_path.file_name().unwrap().to_string_lossy() {
                            std::process::exit(0);
                        }
                    },
                }
            }
        }
        check_if_pid_alive(*target_pid.as_ref().unwrap());
    }
}
