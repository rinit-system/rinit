/* This helper binary is for monitoring dbus changes */
extern crate dbus_bytestream;
extern crate dbus_serialize;
use dbus_bytestream::connection::Connection;
use dbus_bytestream::message;
use dbus_serialize::types::Value::{BasicValue, Array};

extern crate libc;


fn check_dbus_for_name(target_name: &String, conn: &mut Connection) {
    let msg = message::create_method_call(
        "org.freedesktop.DBus", // destination
        "/org/freedesktop/DBus", // path
        "org.freedesktop.DBus", //interface
        "ListNames" // method
    );
    let reply = conn.call_sync(msg).unwrap().unwrap();
    let reply_array = &reply[0];
    match reply_array {
        Array(arr) => {
            for i in &arr.objects {
                match i {
                    BasicValue(dbus_serialize::types::BasicValue::String(s)) => {
                        if s == target_name {
                            println!("Target name {:?} was found", s);
                            std::process::exit(0);
                        }
                    },
                    _ => (),
                }
            }
        },
        _ => (),
    }
}

fn check_if_pid_alive(target_pid: i32) {
    unsafe {
        let res = libc::kill(target_pid, 0);
        if res == -1 {
            println!("The pid {:?} has died", target_pid);
            std::process::exit(1);
        }
    }
}

pub fn main()
{
    let args: Vec<String> = std::env::args().collect();
    let target_name = &args[1];
    let target_pid = &args[2].parse::<i32>();
    let mut conn = Connection::connect_system().unwrap();
    println!("Hello, this is rinit-dbus-monitor, looking for name [{:?}] or the death of pid {:?}",
             target_name, target_pid);
    loop {
        check_dbus_for_name(target_name, &mut conn);
        check_if_pid_alive(*target_pid.as_ref().unwrap());
        std::thread::sleep(std::time::Duration::from_millis(200));
    }
}
