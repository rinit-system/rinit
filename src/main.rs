/* For verifying the process context */
use std::process;
use std::process::Command;
use std::os::unix::process::CommandExt;
use std::process::Stdio;

/* For reading in configuration files */
use std::fs::File;
use std::io::Read;
use std::path::Path;

/* For parsing TOML files */
extern crate toml;
extern crate serde;
use ::serde::de::Deserializer;
use ::serde::Deserialize;
#[macro_use]
extern crate serde_derive;

/* For storing the services */
use std::vec::Vec;

/* For the dependency tree */
extern crate petgraph;
use petgraph::Graph;
use petgraph::dot::{Dot, Config};
use std::collections::HashMap;

/* For monitoring processes */
extern crate libc;
use std::ffi::CString;

/* For communicating with rctl */
use std::os::unix::net::{UnixDatagram, UnixListener, UnixStream, SocketAddr};
use std::io::Write;

/* Shared stuff with rctl and other tools */
mod common;
use common::RinitConfig;

/* Time management */
use std::time;

/* Global variables */
use std::sync::atomic::{AtomicBool, Ordering};
static can_log_now: AtomicBool = AtomicBool::new(false);

#[derive(PartialEq, Eq, Clone, Debug, Deserialize)]
pub enum UnitType {
    Service,
    Mount,
    Task,
}

impl Default for UnitType {
    fn default() -> UnitType { UnitType::Service }
}

#[derive(PartialEq, Eq, Clone, Debug, Deserialize)]
pub enum UnitReady {
    Process,
    Dbus(String),
    File(String),
}
impl Default for UnitReady { fn default() -> Self { UnitReady::Process } }

#[derive(PartialEq, Eq, Clone, Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Unit {
	pub name: String,
        #[serde(default="String::new")]
        pub pre_exec: String,
	pub exec: String,
        pub as_user: String,
        #[serde(default, rename = "Type")]
        pub unit_type: UnitType,
        pub canonical_name: Option<String>,
        pub target: Option<String>,
        #[serde(default, deserialize_with = "__deserialize_ready_when")]
        pub ready_when: UnitReady,
        pub execution: Option<ExecutionDetails>,
        pub behavior: Option<Behavior>,
        pub dependencies: Option<UnitDependencyConfig>,
}

fn __deserialize_ready_when<'de, D>(ds: D)
    -> Result<UnitReady, D::Error>
    where
        D: Deserializer<'de>,
{
    let s: String = String::deserialize(ds)?;
    let rarr: Vec<&str> = s.split(":").collect();
    if rarr.len() == 1 {
        return Ok(UnitReady::Process);
    } else if rarr[0] == "dbus" {
        return Ok(UnitReady::Dbus(rarr[1].clone().to_string()));
    } else if rarr[0] == "file" {
        return Ok(UnitReady::File(rarr[1].clone().to_string()));
    } else {
        panic!(format!("Invalid ReadyWhen: {:?}", rarr));
    }
}

impl Unit {
    pub fn is_canonical_name(name: &String) -> bool {
        name.is_empty() == false && name.contains(".")
    }
    pub fn find_by_canonical_name<'a>(services: &'a Vec<Unit>, cn: &String) -> Option<&'a Unit> {
        services.iter()
            .filter(|x| x.canonical_name.clone().unwrap_or("".to_string()) == *cn)
            .collect::<Vec<_>>()
            .pop()
    }
}

#[derive(PartialEq, Eq, Clone, Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct UnitDependencyConfig {
    pub after_target: Option<String>,
    pub direct: Option<Vec<String>>,
}

#[derive(PartialEq, Eq, Clone, Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct ExecutionDetails {
    #[serde(default)]
    pub stdout_file: String,
    #[serde(default)]
    pub stdin_file: String,
    #[serde(default)]
    pub stderr_file: String,
    #[serde(default)]
    pub rctl_socket_can_be_created_after: bool,
}

#[derive(PartialEq, Eq, Clone, Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Behavior {
    #[serde(default = "__behavior_true")]
    pub auto_restart: bool,
    #[serde(default = "__behavior_true")]
    pub auto_start: bool,
}
fn __behavior_true() -> bool { true }

#[derive(PartialEq, Eq, Clone, Debug)]
enum ServiceState {
    Loaded,
    Started,
    Failed,
    Exited,
    Crashed,
}

#[derive(PartialEq, Eq, Clone, Debug)]
struct ServiceDescriptor {
    pub unit: Unit,
    pub child_pid: u32,
    pub state: ServiceState,
    pub start_time: time::Instant,
    pub failed_start_counter: usize,
}

impl ServiceDescriptor {
    pub fn wait(&mut self) {
        unsafe {
            let status: u32 = std::mem::uninitialized();
            let res = libc::waitpid(self.child_pid as i32,
                          std::mem::transmute(&status),
                          0);
            self.child_pid = 0;
            self.state = ServiceState::Exited;
        }
    }
}

pub fn wait_for_any_children() -> Option<(usize, i32)> {
    let status: i32 = unsafe { std::mem::uninitialized() };

    unsafe {
        let res = libc::waitpid(-1i32,
                        std::mem::transmute(&status),
                        libc::WNOHANG);
        if res == -1 {
            let errno = *libc::__errno_location();
            /* ECHLD */
            if errno == 10 {
                return Some((-1i32 as usize, 0i32));
            }
            return None;
        }

        if libc::WIFEXITED(status) {
            return Some((res as usize, libc::WEXITSTATUS(status)));
        } else {
            return Some((res as usize, -1i32));
        }
    }
}

pub fn is_pid_alive(pid: i32) -> bool {
    let res: i32;
    let errno: i32;
    unsafe {
        res = libc::kill(pid, 0);
        errno = *libc::__errno_location();
    }

    return !(res == -1 && errno == libc::ESRCH);
}

/* This function assumes that srv.state == Started */
fn do_gentle_stop_service(srv: &ServiceDescriptor) {
    unsafe {
        libc::kill(srv.child_pid as i32, libc::SIGTERM);
    }
}

fn do_force_stop_service(srv: &ServiceDescriptor) {
    unsafe {
        libc::kill(srv.child_pid as i32, libc::SIGKILL);
    }
}

/* This function assumes that srv.state == Started */
fn stop_service(srv: &ServiceDescriptor) -> ServiceDescriptor {
    /* stop the service */
    do_gentle_stop_service(srv);

    /* check if the service has died */
    if is_pid_alive(srv.child_pid as i32) {
        /* didn't exit yet, wait a little */
        std::thread::sleep(std::time::Duration::from_millis(300));

        /* still alive? */
        if is_pid_alive(srv.child_pid as i32) {
            /* if so, force kill */
            do_force_stop_service(srv);
        }
    }

    /* mark the descriptor as exited */
    ServiceDescriptor {
        state: ServiceState::Exited,
        child_pid: 0,
        .. srv.clone()
    }
}

fn logging_setup() {
    println!("[LOGGING] turning logging on");
    can_log_now.store(true, Ordering::SeqCst);
}

fn rotate_log(p: &Path) -> Stdio {
    /* TODO: log rotation */
    create_log(p)
}

fn create_log(p: &Path) -> Stdio {
    let parent = p.parent().unwrap();
    if !parent.exists() {
        std::fs::create_dir(parent);
    }
    let file = File::create(p);
    Stdio::from(file.unwrap())
}

fn do_log(p: &Path) -> Stdio {
    if p.exists() {
        rotate_log(p)
    } else {
        create_log(p)
    }
}

fn logging_setup_for(unit_name: &String) -> (Stdio, Stdio, Stdio) {
    let stdin: Stdio;
    let stdout: Stdio;
    let stderr: Stdio;
    let should_log = can_log_now.load(Ordering::SeqCst);

    if unit_name == "" {
        return (Stdio::null(), Stdio::null(), Stdio::null());
    }

    if should_log {
        let stdout_path_str = format!("/var/log/services/{}/stdout.log", unit_name);
        let stdout_path = Path::new(&stdout_path_str);
        let stderr_path_str = format!("/var/log/services/{}/stderr.log", unit_name);
        let stderr_path = Path::new(&stderr_path_str);

        stdin = Stdio::inherit();
        stdout = do_log(&stdout_path);
        stderr = do_log(&stderr_path);
    } else {
        stdin = Stdio::inherit();
        stdout = Stdio::inherit();
        stderr = Stdio::inherit();
        println!("[LOGGING] Skipping logging for {}", unit_name);
    }

    (stdin, stdout, stderr)
}

fn start_service(unit: &Unit) -> (ServiceDescriptor, bool) {
    let args: Vec<&str> = unit.exec.split_whitespace().collect();
    //println!("[INFO] Starting service {:?}", unit);
    /* check if there is a pre-exec hook */
    if unit.pre_exec != "".to_string() {
        let pre_exec_args: Vec<&str> = unit.pre_exec.split_whitespace().collect();
        let mut pre_exec_child = Command::new(&pre_exec_args[0]).args(&pre_exec_args[1..]).spawn();
        match &mut pre_exec_child {
            Ok(c) => { c.wait(); },
            Err(_) => (),
        }
    }
    let stdin_file: Stdio;
    let stdout_file: Stdio;
    let stderr_file: Stdio;
    let should_setsid: bool;

    match &unit.execution {
        Some(exec) => {
            /* detach from the CTTY */
            let stdin_path = Path::new(&exec.stdin_file);
            let stdout_path = Path::new(&exec.stdout_file);
            let stderr_path = Path::new(&exec.stderr_file);
            stdin_file = if stdin_path.exists() {
                    File::open(stdin_path).unwrap().into()
                } else { Stdio::inherit() };
            stdout_file = if stdout_path.exists() {
                    File::create(stdout_path).unwrap().into()
                } else { Stdio::inherit() };
            stderr_file = if stderr_path.exists() {
                    File::create(stderr_path).unwrap().into()
                } else { Stdio::inherit() };

            /* Allow the service to acquire a controlling terminal */
            should_setsid = true;
        },
        None => {
            let (a, b, c) = logging_setup_for(&unit.canonical_name.as_ref().unwrap_or(&"".to_string()));
            stdin_file = a;
            stdout_file = b;
            stderr_file = c;
            should_setsid = false;
        }
    };

    /* spawn the child */
    let child = Command::new(&args[0])
        .args(&args[1..])
        .stdin(stdin_file)
        .stdout(stdout_file)
        .stderr(stderr_file)
        .before_exec(move || {
            if should_setsid {
                unsafe { libc::setsid() };
            }
            Ok(())
        })
        .spawn();

    match unit.ready_when {
        UnitReady::Dbus(ref name) => {
            let s = Command::new("/usr/bin/rinit-dbus-monitor")
                        .args(vec![name,
                                    &child.as_ref()
                                     .map(|x| x.id())
                                     .unwrap_or(0)
                                     .to_string()])
                        .status();
            match s {
                Ok(stat) => {
                    if !stat.success() {
                        return (ServiceDescriptor {
                                start_time: time::Instant::now(),
                                failed_start_counter: 1,
                                unit: unit.clone(),
                                child_pid: 0,
                                state: ServiceState::Failed,
                            }, false);
                    }
                },
                Err(_) => {
                    return (ServiceDescriptor {
                            start_time: time::Instant::now(),
                            failed_start_counter: 1,
                            unit: unit.clone(),
                            child_pid: 0,
                            state: ServiceState::Failed,
                    }, false);
                },
            }

        },
        UnitReady::File(ref name) => {
            let s = Command::new("/usr/bin/rinit-file-monitor")
                        .args(vec![name,
                                    &child.as_ref()
                                     .map(|x| x.id())
                                     .unwrap_or(0)
                                     .to_string()])
                        .status();
            match s {
                Ok(stat) => {
                    if !stat.success() {
                        return (ServiceDescriptor {
                                start_time: time::Instant::now(),
                                failed_start_counter: 1,
                                unit: unit.clone(),
                                child_pid: 0,
                                state: ServiceState::Failed,
                            }, false);
                    }
                },
                Err(e) => {
                    return (ServiceDescriptor {
                            start_time: time::Instant::now(),
                            failed_start_counter: 1,
                            unit: unit.clone(),
                            child_pid: 0,
                            state: ServiceState::Failed,
                    }, false);
                },
            }
        },
        UnitReady::Process => {
            ()
        },
    }
    //println!("[INFO]    -> {:?}", child);
    match &child {
        Ok(c) => { (ServiceDescriptor {
                    start_time: time::Instant::now(),
                    failed_start_counter: 0,
                    unit: unit.clone(),
                    child_pid: c.id(),
                    state: ServiceState::Started,
                }, true) },
        Err(_) => { (ServiceDescriptor {
                    start_time: time::Instant::now(),
                    failed_start_counter: 1,
                    unit: unit.clone(),
                    child_pid: 0,
                    state: ServiceState::Failed,
                }, false) },
    }
}

fn start_services(services: &Vec<Unit>, plan: Vec<&String>) -> Vec<ServiceDescriptor> {
    let mut descriptors: Vec<ServiceDescriptor> = Vec::new();
    let services_plan: Vec<&Unit> = plan.iter()
        .filter(|x| Unit::is_canonical_name(x))
        .map(|x| Unit::find_by_canonical_name(services, x).unwrap())
        .collect();
    let stalled_services: Vec<&Unit> = services.iter()
        .filter(|x| x.behavior.as_ref().map_or(false, |b| !b.auto_start))
        .collect();

    for unit in services_plan {
        let (mut desc, did_start) = start_service(unit);
        let should_wait = did_start && unit.unit_type != UnitType::Service;
        if should_wait {
            desc.wait();
        }
        descriptors.push(desc);
    }

    for unit in stalled_services {
        descriptors.push(ServiceDescriptor {
            start_time: time::Instant::now(),
            failed_start_counter: 0,
            unit: unit.clone(),
            child_pid: 0,
            state: ServiceState::Loaded,
        });
    }

    descriptors
}

fn enumerate_services(cfg: &RinitConfig) -> Vec<Unit>
{
    let mut services: Vec<Unit> = Vec::new();

    /* Iterate through the files in the ServicesDirectory */
    let services_directory_path: &Path = Path::new(&cfg.services_directory);
    let i = std::fs::read_dir(services_directory_path).unwrap();
    for entry in i {
        let path = entry.unwrap().path();
        if path.is_file() {
            let mut fp = File::open(path).unwrap();
            let mut cts = String::new();
            fp.read_to_string(&mut cts).expect("Unable to read from unit");
            let unit: Unit = toml::from_str(&cts).unwrap();

            services.push(unit);
        }
    }

    services
}

fn handle_service_death(unit: &Unit) -> ServiceDescriptor {
    /* Check the behavior */
    let behavior = &unit.behavior;

    match behavior {
        None => {
            /* default behavior */
            match unit.unit_type {
                UnitType::Service => {
                    /* default behavior for services is to restart it */
                    let (desc, _) = start_service(unit);
                    return desc;
                },
                _ => {
                    /* other tasks are not automatically restarted */
                    return ServiceDescriptor {
                        failed_start_counter: 0,
                        start_time: time::Instant::now(),
                        unit: unit.clone(),
                        child_pid: 0,
                        state: ServiceState::Exited,
                    }
                },
            }
        },
        Some(behave) => {
            if behave.auto_restart {
                let (desc, _) = start_service(unit);
                return desc;
            } else {
                return ServiceDescriptor {
                    failed_start_counter: 0,
                    start_time: time::Instant::now(),
                    unit: unit.clone(),
                    child_pid: 0,
                    state: ServiceState::Exited,
                }
            }
        },
    }
}

fn find_descriptor_for_pid(descs: &Vec<ServiceDescriptor>, pid: u32) -> Option<&ServiceDescriptor> {
    descs.iter()
        .filter(|x| x.child_pid == pid as u32)
        .collect::<Vec<_>>()
        .pop()
}

fn handle_death(descriptors: &mut Vec<ServiceDescriptor>,
                pid: usize,
                retcode: i32)
                    -> Vec<ServiceDescriptor>{
    println!("[INFO] Something with pid {} died, retcode: {}!", pid, retcode);

    let is_error = retcode != 0;

    /* TODO: Come back to this when non-lexical lifetimes are stable */
    /* find the descriptor corresponding to the pid */
    let desc = find_descriptor_for_pid(descriptors, pid as u32);
    let mut new_descriptors: Vec<ServiceDescriptor> = Vec::new();

    new_descriptors.clone_from(descriptors);
    new_descriptors.retain(|x| x.child_pid != pid as u32);

    match desc {
        None => {
            println!("[WARN] Received a wait event for unknown pid {}", pid);
            return descriptors.clone();
        },
        Some(descriptor) => {
            println!("[WARN] Service {} has exited", descriptor.unit.name);
            if descriptor.state != ServiceState::Exited {
                if descriptor.failed_start_counter < 3 {
                    let mut dd = handle_service_death(&descriptor.unit);
                    dd.failed_start_counter += descriptor.failed_start_counter;
                    if is_error {
                        dd.failed_start_counter += 1;
                    }
                    println!("failed_start_counter is now {}", dd.failed_start_counter);
                    new_descriptors.push(dd);
                } else {
                    new_descriptors.push(ServiceDescriptor {
                        state: ServiceState::Crashed,
                        .. descriptor.clone()
                        });
                }
            }
            return new_descriptors;
        },
    }
}

fn handle_rctl_sys_services(descriptors: &Vec<ServiceDescriptor>,
                            sock: &mut UnixStream)
                                -> Vec<ServiceDescriptor> {
    let mut final_string = String::new();

    for i in descriptors {
        let s = format!("{{\"canonical_name\":\"{}\", \"last_pid\":{}, \"state\":\"{:?}\", \"name\":\"{}\"}}\n",
                     i.unit.canonical_name.clone().unwrap_or(i.unit.name.clone()), i.child_pid, i.state,
                     i.unit.name.clone());
        final_string += &s;
    }

    sock.write(final_string.as_bytes());

    /* there are no changes that need to be committed */
    Vec::new()
}

fn handle_rctl_start_service(descriptors: &Vec<ServiceDescriptor>,
                             sock: &mut UnixStream,
                             arg: &String)
                                -> Vec<ServiceDescriptor> {
    let changes: Vec<ServiceDescriptor>;
    let service = descriptors.iter()
        .filter(|x| x.unit.canonical_name.as_ref().unwrap_or(&"".to_string()) == arg)
        .collect::<Vec<_>>()
        .pop();

    println!("Starting service: {}", arg);

    if service.is_some() {
        let srv = service.unwrap();

        let (mut change, _) = start_service(&srv.unit);
        change.failed_start_counter += srv.failed_start_counter;
        changes = vec![change];
    } else {
        /* there are no changes that need to be committed */
        changes = Vec::new()
    }

    changes
}

fn handle_rctl_stop_service(descriptors: &Vec<ServiceDescriptor>,
                             sock: &mut UnixStream,
                             arg: &String)
                                -> Vec<ServiceDescriptor> {
    let changes: Vec<ServiceDescriptor>;
    let service = descriptors.iter()
        .filter(|x| x.unit.canonical_name.as_ref().unwrap_or(&"".to_string()) == arg)
        .collect::<Vec<_>>()
        .pop();

    if service.is_some() {
        let srv = service.unwrap();
        if srv.state == ServiceState::Started {
            let change = stop_service(&srv);
            changes = vec![change];
        } else {
            changes = Vec::new()
        }
    } else {
        /* there are no changes that need to be committed */
        changes = Vec::new()
    }

    changes
}

fn handle_rctl_command(descriptors: &Vec<ServiceDescriptor>,
                       cmd: &String,
                       sock: &mut UnixStream)
                        -> Vec<ServiceDescriptor> {
    let split_cmd = (*cmd).split_whitespace().collect::<Vec<_>>();
    let changes: Vec<ServiceDescriptor>;
    if split_cmd[0] == "SYS_SERVICES".to_string() {
        changes = handle_rctl_sys_services(descriptors, sock);
    } else if split_cmd[0] == "START_SERVICE".to_string() {
        if split_cmd.len() >= 2 {
            changes = handle_rctl_start_service(&descriptors, sock, &split_cmd[1].to_string());
        } else {
            changes = Vec::new();
        }
    } else if split_cmd[0] == "STOP_SERVICE".to_string() {
        if split_cmd.len() >= 2 {
            changes = handle_rctl_stop_service(&descriptors, sock, &split_cmd[1].to_string());
        } else {
            changes = Vec::new();
        }
    } else {
        changes = Vec::new();
        println!("received a command: [{}]", split_cmd[0]);
    }
    changes
}

fn handle_rctl_client(descriptors: &Vec<ServiceDescriptor>,
                      sock: &mut UnixStream,
                      addr: &SocketAddr)
                        -> Vec<ServiceDescriptor> {
    let mut buf = vec![0; 32];

    sock.read(buf.as_mut_slice());

    handle_rctl_command(descriptors,
                    &String::from_utf8_lossy(buf.as_mut_slice()).into_owned(), sock)
}

fn handle_rctl_requests(descriptors: &Vec<ServiceDescriptor>,
                        sock: &UnixListener)
                            -> Vec<ServiceDescriptor> {
    match sock.accept() {
        Ok((mut client_sock, client_addr)) => handle_rctl_client(descriptors, &mut client_sock, &client_addr),
        Err(e) => {
            if e.kind() != std::io::ErrorKind::WouldBlock {
                println!("Error reading from socket: {:?}", e);
            }
            Vec::new()
        },
    }
}

fn descriptor_apply_changes(descriptors: Vec<ServiceDescriptor>,
                            changes: Vec<ServiceDescriptor>)
                                -> Vec<ServiceDescriptor> {
    let mut ret: Vec<ServiceDescriptor> = descriptors.clone();

    for change in changes {
        ret = ret.iter()
            .map(|x| if x.unit.canonical_name == change.unit.canonical_name {
                change.clone() } else { x.clone() })
            .collect::<Vec<_>>();
    }

    ret
}

fn create_rctl_socket(socket_path: &String) -> Option<UnixListener> {
        let socket = match UnixListener::bind(socket_path) {
            Ok(sock) => Some(sock),
            Err(e) => {
                println!("[ERR] Socket path \"{}\" unavailable due to: {}", socket_path, e);
                println!("      rctl and other tools will be unavailable.");
                None
            }
        };

        match &socket {
            Some(sock) => sock.set_nonblocking(true).expect("failed to set domain to nonblock mode"),
            None => (),
        };

        socket
}

fn main() {
        /* Force RUST_BACKTRACE=1 */
        //std::env::set_var("RUST_BACKTRACE", "1");
        let mut base_path: String = "/usr/share/rinit".to_string();
        let args: Vec<String> = std::env::args().collect();
        if args.len() == 2 {
            base_path = args[1].clone();
        }

        /* XXX: create /dev/urandom */
        if !Path::new("/dev/urandom").exists() {
            unsafe {
                let mut res = 0;
                res = libc::mknod(CString::new("/dev/urandom").unwrap().as_ptr(),
                                    libc::S_IFCHR,
                                    libc::makedev(1, 9));
                res = libc::mknod(CString::new("/dev/random").unwrap().as_ptr(),
                                    libc::S_IFCHR,
                                    libc::makedev(1, 8));
            }
        }

	/* TODO: There should be a separate config that validates the config to avoid potentially crashing pid 1 */
	/* TODO: move this to separate file */
	/* Parse configuration */
        let mut configfile = File::open(base_path.clone() + "/rinit.toml")
            .expect(&format!("Configuration file {}/rinit.toml not found, is /usr mounted?", &base_path));
        let mut cfile_cts = String::new();
        configfile.read_to_string(&mut cfile_cts).expect("Unable to read configuration file");
        let cfg: RinitConfig = toml::from_str(&cfile_cts).unwrap();

        println!("[INFO] This is rinit v0.0.1-git, target goal: {}", cfg.goal);

        /* Do the work */
        let all_services = enumerate_services(&cfg);
        let services = all_services.clone().into_iter()
                .filter(|s| s.behavior.as_ref()
                        .map_or(true, |b| b.auto_start))
                .collect::<Vec<_>>();
        let mut targets: Vec<String> = services.iter()
                        .map(|x| x.target.clone().unwrap_or("".to_string()))
                        .filter(|x| x != "")
                        .collect();
        targets.sort();
        targets.dedup();

        let (should_create_socket_before, create_socket_after_cn): (bool, String) =
            all_services.iter()
                .filter(|srv| srv.execution.as_ref()
                        .map(|x| x.rctl_socket_can_be_created_after)
                        .unwrap_or(false))
                .collect::<Vec<_>>()
                .pop()
                .map(|srv| (false, srv.canonical_name.clone().unwrap_or("".to_string())))
                .unwrap_or((true, "".to_string()));

        let raw_dependencies: Vec<(String, String)> =
            services.iter()
            .map(|x| (if x.dependencies.is_some() {
                        x.dependencies.clone().unwrap().after_target.unwrap_or("".to_string())
                      } else { "".to_string() },
                        x.target.clone().unwrap()
                      ))
            .collect();

        let direct_dependencies: Vec<(Vec<String>, String)> =
            services.iter()
            .map(|x| (if x.dependencies.is_some() {
                  x.dependencies.clone().unwrap().direct.unwrap_or([].to_vec())
                } else { [].to_vec() },
                  x.canonical_name.clone().unwrap()
                ))
            .collect();

        /* Construct the dependency tree */
        let mut deps = Graph::<String, ()>::new();

	/* Add targets */
	let mut dep_targets: HashMap<String, petgraph::prelude::NodeIndex>
		= targets.iter().map(|x| (x.clone(), deps.add_node(x.to_string()))).collect();

        /* Add relations between targets */
	for (dependency, current) in &raw_dependencies {
            /* if the current has no target dependency, then skip */
            if dependency == "" {
                    continue;
            }
            /* find the nodeindex for the current */
            let mut curr_idx: &petgraph::prelude::NodeIndex = dep_targets.get(current).unwrap();

            /* find the nodeindex for the dependency */
            let mut dep_idx: &petgraph::prelude::NodeIndex = dep_targets.get(dependency).unwrap();

            /* Finally, add the dependency to the graph */
            deps.add_edge(*dep_idx, *curr_idx, ());
	}

	println!("{:?}", Dot::with_config(&deps, &[Config::EdgeNoLabel]));

        let res = petgraph::algo::toposort(&deps, None);
        let mut target_plan = match res {
            Ok(ls) => ls.iter()
                        .map(|x| deps.node_weight(*x))
                        .map(|x| x.unwrap())
                        .collect::<Vec<_>>(),
            Err(_) => Vec::new(),
        };

        let mut _plan: Vec<String> = Vec::new();

        let (_, n_target_plan): (bool, Vec<String>) = target_plan.iter()
            .fold((false, vec![]), |(done, mut ret), x|
                  if done { (done, ret) } else {
                      ret.push(x.clone().to_string());
                      (*x == &cfg.goal, ret)
                  });

        println!("Target plan: {:?}", &n_target_plan);

        /* Now, for each target, create its own dependency graph */
        for i in n_target_plan {
            /* create the graph */
            let mut t_deps = Graph::<String, ()>::new();

            /* find the services that form part of this target and add them
             * to the graph
             */
            let target_services: HashMap<String, (Unit, petgraph::prelude::NodeIndex)>
                    = all_services.iter()
                        .filter(|x| Unit::is_canonical_name(x.canonical_name
                                                            .as_ref().unwrap_or(&"".to_string())))
                        .filter(|x| x.target == Some(i.to_string()))
                        .map(|x| (x.canonical_name.clone(), (x.clone(),
                                    t_deps.add_node(x.canonical_name.as_ref().unwrap_or(&"".to_string()).to_string()))))
                        .map(|(a, b)|  (a.unwrap_or("".to_string()), b))
                        .collect();

            /* Add dependencies between the units in this target */
            for (cn, (unit, node)) in &target_services {
                if unit.dependencies.is_none() || unit.dependencies.as_ref().unwrap().direct.is_none() {
                    continue;
                }

                let u_deps = &unit.dependencies.as_ref().unwrap().direct.clone().unwrap();
                for u_dep in u_deps.iter() {
                    println!("i: {} cn: {} Direct dep: {:?}", i, cn, u_dep);
                    let (u_udep, u_udep_idx) = target_services.get(u_dep).unwrap();
                    println!("Success");
                    t_deps.add_edge(*u_udep_idx, *node, ());
                }
            }

            let t_res = petgraph::algo::toposort(&t_deps, None);
            let mut t_plan = match t_res {
                Ok(ls) => ls.iter()
                            .map(|x| t_deps.node_weight(x.clone()).unwrap())
                            .map(|x| (*x).clone())
                            .collect::<Vec<_>>(),
                Err(_) => Vec::new(),
            };
            _plan.append(&mut t_plan);
        }

        println!("COOOKIES");
        
        let plan = _plan.iter().map(|x| x).collect::<Vec<_>>();

        if plan.is_empty() {
            println!("Cycle detected in dependency graph. Note that this may be two things:");
            println!("1) A bug in rinit. This is likely.");
            println!("2) A bug in the configuration. :-) Fix it.");
            println!("rinit will be able to solve this problem in the future, so check back soon!");
            return;
        }

        let socket: Option<UnixListener>;
        let mut descriptors: Vec<ServiceDescriptor>;

        println!("Startup plan: {:?}", plan);

        if should_create_socket_before {
            socket = create_rctl_socket(&cfg.socket_path);
            descriptors = start_services(&all_services, plan);
        } else {
            let pos = plan.iter()
                        .position(|&r| *r == create_socket_after_cn)
                        .unwrap() + 1;

            let (before, after) = plan.split_at(pos);
            let mut before_descs = start_services(&all_services, before.into());

            logging_setup();
            socket = create_rctl_socket(&cfg.socket_path);

            let mut after_descs = start_services(&all_services, after.into());

            before_descs.append(&mut after_descs);
            descriptors = before_descs;
        }

        //println!("[INFO] All services have been started: {:?}", &descriptors);
        loop {
            /* block, waiting for a wait-event from something */
            let wait_res = wait_for_any_children();

            if wait_res.is_some() {
                let (p, retcode) = wait_res.unwrap();
                if p != (-1i32 as usize)  && p != 0 {
                    descriptors = handle_death(&mut descriptors, p, retcode);
                }
            }

            /* see if there has been any socket communication */
            if socket.is_some() {
                let changes = handle_rctl_requests(&descriptors, &socket.as_ref().unwrap());
                descriptors = descriptor_apply_changes(descriptors, changes);
            }
        }
}
