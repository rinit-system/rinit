
extern crate toml;
#[macro_use]
extern crate serde_derive;

/* For I/O */
use std::fs::File;
use std::io::Read;

/* For parsing the config */
mod common;
use common::RinitConfig;

/* For connecting to the rctl socket */
use std::os::unix::net::UnixStream;
use std::io::prelude::*;

/* For parsing the socket replies */
extern crate json;

fn rctl_status(cfg: &RinitConfig, target: Option<&String>) {
    /* connect to the socket */
    let mut stream = UnixStream::connect(&cfg.socket_path)
        .expect(&format!("Cannot connect to the UNIX STREAM socket at {}",
                &cfg.socket_path));

    stream.write_all("SYS_SERVICES\n".to_string().into_bytes().as_slice()).unwrap();
    let mut response = String::new();
    stream.read_to_string(&mut response).unwrap();
    let services = response.lines().collect::<Vec<_>>();

    match target {
        None => {
            for i in services {
                let service = json::parse(i).unwrap();
                if service["state"].to_string() == "Started".to_string() {
                    println!("Service [{}] {} -> Last known PID: {}",
                             service["canonical_name"], service["state"], service["last_pid"]);
                } else {
                    println!("Service [{}] {}",
                             service["canonical_name"], service["state"]);
                }
            }
        },
        Some(trg) => {
            for i in services {
                let j = json::parse(i).unwrap();
                let is_alive = j["state"].to_string() == "Started".to_string();
                if j["canonical_name"].to_string() == *trg {
                    println!("• {} - {}", j["canonical_name"], j["name"]);
                    println!("   Status: {}", j["state"].to_string());
                    if is_alive {
                        println!(" Main PID: {}", j["last_pid"].to_string());
                    }
                    return;
                }
            }

            /* TODO: check in AvailableServicesDirectory and recommend loading */
            println!("Unknown target!");
        },
    }
}

fn rctl_start_service(cfg: &RinitConfig, srv: &String) {
    /* connect to the socket */
    let mut stream = UnixStream::connect(&cfg.socket_path)
        .expect(&format!("Cannot connect to the UNIX STREAM socket at {}",
                &cfg.socket_path));

    stream.write_all(format!("START_SERVICE {}\n", srv).to_string().into_bytes().as_slice()).unwrap();
}

fn rctl_stop_service(cfg: &RinitConfig, srv: &String) {
    /* connect to the socket */
    let mut stream = UnixStream::connect(&cfg.socket_path)
        .expect(&format!("Cannot connect to the UNIX STREAM socket at {}",
                &cfg.socket_path));

    stream.write_all(format!("STOP_SERVICE {}\n", srv).to_string().into_bytes().as_slice()).unwrap();
}

fn rctl_unknown_command(_cfg: &RinitConfig) {
    /* do usage */
    println!("Unknown command!");
}

fn rctl_usage(_cfg: &RinitConfig) {
    println!("rctl - introspection and manipulation tool for rinit");
    println!("");
    println!("Available commands:");
    println!("  status [service_cn]     Shows the status of the services running on the system,");
    println!("                          or the service with CN service_cn");
    println!("  start  service_cn       Starts the service with CN service_cn.");
    println!("  stop   service_cn       Stops the service with CN service_cn.");
}

pub fn main() {
    let mut configfile = File::open("/usr/share/rinit/rinit.toml").expect("Configuration file /usr/share/rinit/rinit.toml not found, is /usr mounted?");
    let mut cfile_cts = String::new();
    configfile.read_to_string(&mut cfile_cts).expect("Unable to read configuration file");
    let cfg: RinitConfig = toml::from_str(&cfile_cts).unwrap();

    let args: Vec<String> = std::env::args().collect();

    if args.len() == 1 {
        rctl_usage(&cfg);
        return;
    }

    if args[1] == "status".to_string() {
        if args.len() == 3 {
            rctl_status(&cfg, Some(&args[2]));
        } else {
            rctl_status(&cfg, None);
        }
    } else if args[1] == "start".to_string() {
        if args.len() == 3 {
            rctl_start_service(&cfg, &args[2]);
        } else {
            println!("Specify a CN to start!");
        }
    } else if args[1] == "stop".to_string() {
        if args.len() == 3 {
            rctl_stop_service(&cfg, &args[2]);
        } else {
            println!("Specify a CN to stop!");
        }
    } else {
        rctl_unknown_command(&cfg);
    }
}
