#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct RinitConfig {
    pub logging_level: usize,
    pub services_directory: String,
    pub goal: String,
    #[serde(default = "__default_socket_path")]
    pub socket_path: String,
}

fn __default_socket_path() -> String { "/usr/share/rinit/rctl.sock".to_string() }
